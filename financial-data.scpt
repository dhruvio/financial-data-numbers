JsOsaDAS1.001.00bplist00�Vscript_�var config = {
	tableName: "Financial Data",
	priceCellColumnAdjustment: 1,
	priceCellRowAdjustment: 0
};

var app = Application.currentApplication(),
		numbers = Application("Numbers");

app.includeStandardAdditions = true;

// loop over an array
var forEach = function(array, callback) {
	for (var i = 0; i < array.length; i++) {
		callback(array[i], i, array);
	}
};

// Get the price of a ticker from Yahoo finance
var getPrice = function (ticker) {
	if (!ticker || typeof ticker !== "string") {
		return undefined;
	}
	
	var json = app.doShellScript("curl -s http://finance.yahoo.com/webservice/v1/symbols/" + ticker + "/quote?format=json", {
				administratorPrivileges: false
			}),
			obj = JSON.parse(json),
			found = !!obj.list.meta.count,
			price = found ? obj.list.resources[0].resource.fields.price : undefined;
			
	return price;
};


// gets the tables with tickers and returns an array of them
var getTables = function(documents) {
	var tables = [];

	forEach(documents, function(document) {
		forEach(document.sheets, function(sheet) {
			var tablesToAdd = Array.prototype.slice.call(sheet.tables.whose({name: config.tableName}), 0);
			tables = tables.concat(tablesToAdd || []);
		});
	});

	return tables;
};

var findCellAtCoordinates = function(cells, column, row) {
	var cell,
			temp,
			i = 0;
	
	while (!cell && i < cells.length) {
		temp = cells.at(i);
		if (temp.column().address() === column && temp.row().address() === row) {
			cell = temp;
		}
		i++;
	}
	
	return cell;
}

var tables = getTables(numbers.documents);

forEach(tables, function(table) {
  var rowCount = table.rowCount(),
			columnCount = table.columnCount(),
			headerRowCount = table.headerRowCount(),
			headerColumnCount = table.headerColumnCount(),
			footerRowCount = table.footerRowCount(),
			rowStart = 1 + headerRowCount,
			rowEnd = rowCount - footerRowCount,
			columnStart = 1 + headerColumnCount,
			columnEnd = columnCount,
			cells = table.cells;

	forEach(cells, function(cell) {
		var columnAddress = cell.column().address(),
				rowAddress = cell.row().address(),
				value = cell.value();

		if (columnAddress < columnStart ||
				columnAddress > columnEnd ||
				rowAddress < rowStart ||
				rowAddress > rowEnd ||
				!value) {
			return;
		}

		if (config.priceCellColumnAdjustment > 0 &&
				columnAddress > columnEnd - config.priceCellColumnAdjustment) {
			return;
		}

		if (config.priceCellRowAdjustment > 0 &&
				rowAddress > rowEnd - config.priceCellRowAdjustment) {
			return;
		}

		if (config.priceCellColumnAdjustment < 0 &&
				columnAddress < columnStart - config.priceCellColumnAdjustment) {
			return;
		}

		if (config.priceCellRowAdjustment < 0 &&
				rowAddress < rowStart - config.priceCellRowAdjustment) {
			return;
		}
		
		var price = getPrice(value);

		if (!price) {
			return;
		}

		var priceCell = findCellAtCoordinates(cells, columnAddress + config.priceCellColumnAdjustment, rowAddress + config.priceCellRowAdjustment);
		priceCell.value = price;
	});
});
                              � jscr  ��ޭ