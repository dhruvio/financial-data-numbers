# Financial Data Script for Numbers

A JXA script that, when run, pulls the latest financial data from Yahoo Finance into *Numbers* on *Mac OS X Yosemite*.

## Example

| Ticker | Price |
|---|---|
| BTCUSD=X | 293.619995 |
| USDCAD=X | 1.272750 |
| TSLA | 191.070007 |

## Installation Instructions

1. Download the `financial-data.scpt` file, and paste it into `~/Library/Scripts/Applications/Numbers/`.

2. Open the *Script Editor* app (located at `/Applications/Utilities/Script Editor.app`).

3. Under *Preferences* for *Script Editor*, make sure *Show Script menu in menu bar* is selected. You can now quit *Script Editor*.

4. You can now run the `financial-data` script inside *Numbers* by clicking on the *Scripts* icon in the menu bar (near the clock, it looks like a squiggly piece of paper).

## Usage Instructions

1. Open an existing, or create a new, *Numbers* document.

2. Create a table with the title: *Financial Data* (case is important).

3. Inside the table, add Yahoo tickers (e.g. `BTCUSD=X`) in a **non-header and non-footer cell**. Ensure there is a blank cell to the right of the ticker.

4. Run the `financial-data` script from the Scripts menu.

**Hint:** This works if you have more than one document open in Numbers, with multiple tables titled *Financial Data*!

## Customisation

By default, this script will only insert financial prices if a cell is available to the right of a valid, Yahoo Finance ticker in a table titled *Financial Data*.

However, you can customise the name of the table that the script looks for to insert the data, as well as where the price is inserted into the table relative to a ticker.

To do so, simply edit the following attributes inside the `config` options hash at the beginning of the script file to your preference:

| Attribute | Description | Value Type |
|---|---|---|
| `tableName` | The name of the table to search for. | String |
| `priceCellColumnAdjustment` | The column the price should be inserted into relative to the ticker | Integer, negative numbers are left, positive numbers are right |
| `priceCellRowAdjustment` | The row the price should be inserted into relative to the ticker | Integer, negative numbers are up, positive numbers are down |

## Author

Dhruv Dang
<br>
http://dhruv.io
<br>
hi@dhruv.io

## License

MIT
